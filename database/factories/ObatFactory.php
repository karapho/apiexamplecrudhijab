<?php

namespace Database\Factories;

use App\Models\Obat;
use Illuminate\Database\Eloquent\Factories\Factory;

class ObatFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Obat::class;

    public function definition()
    {
        $text = [
            "kode_obat"=> $this->faker->bothify('??###'),
            "nama_obat"=>$this->faker->word,
            "jenis_obat"=>$this->faker->randomElement($array = array ('Obat Ringan','Obat Normal','Obat Keras')),
            "harga"=> $this->faker->numberBetween($min = 1000, $max = 999999)
        ];

        return $text;
    }
}
