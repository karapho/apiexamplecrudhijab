<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ObatController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth',
], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);
    Route::post('ttl', [AuthController::class, 'ttl']);
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'v1'
], function () {
    Route::get('obat', [ObatController::class,'index']);
    Route::get('obat/{id}', [ObatController::class,'show']);
    Route::post('obat', [ObatController::class,'store']);
    Route::delete('obat/{id}',[ObatController::class, 'destroy']);
    Route::put('obat/{id}',[ObatController::class, 'update']);
    Route::patch('obat/{id}',[ObatController::class, 'patch']);
});