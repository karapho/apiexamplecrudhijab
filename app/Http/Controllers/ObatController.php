<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreObatRequest;
use App\Http\Requests\UpdateObatRequest;
use Illuminate\Http\Request;
use App\Models\Obat;
use App\Http\Requests\ObatRequest;

class ObatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Obat::paginate(10);
        return response()->json(['status'=>200,'data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreObatRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ObatRequest $request)
    {
        $data = $request->all();

        $cek = Obat::create($data);

        if ($cek->exists) {
        return response()->json(['status'=>200,'message'=>'Data Successfully Created','data'=>$request->all()]);
        // success
         } else {
            // failure 
            return response()->json(['status'=>'Gagal Simpan Data ke DB']);
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Obat  $obat
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obat = Obat::find($id);
        if (!$obat) {
            return response()->json(
                [
                    'Message' => 'Data Not Found',
                ],
                400);
        }
        // return $obat ;

        return response()->json(['status'=>200,'data'=>$obat]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Obat  $obat
     * @return \Illuminate\Http\Response
     */
    public function edit(Obat $obat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateObatRequest  $request
     * @param  \App\Models\Obat  $obat
     * @return \Illuminate\Http\Response
     */
    public function update(ObatRequest $request, $id)
    {
        $data = $request->all();

        $item = Obat::find($id);
        if (!$item) {
            return response()->json(
                [
                    'Message' => 'Data Not Found',
                ],
                400);
        }

        $item->update($data);

        return response()->json(['status'=>200,'message'=>'Data Successfully Updated','data'=>$item]);
    }

    public function patch(ObatRequest $request, $id)
    {
        $data = $request->all();
        $item = Obat::find($id);

        // $this->validate(PackageRequest $data);

        if (!$item) {
            return response()->json(
                [
                    'Message' => 'Data Not Found',
                ],
                400);
        }

        $item->update($data);

        return response()->json(['status'=>200,'message'=>'Data Successfully Updated','data'=>$item]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Obat  $obat
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $obat = Obat::find($id);
        if (!$obat) {
            return response()->json(
                [
                    'Message' => 'Data Not Found',
                ],
                400);
        }
        $data = $obat;
        $obat->delete();

        return response()->json(['status'=>200,'message'=>'Data Successfully Deleted', 'data'=>$obat]);
    }
}
