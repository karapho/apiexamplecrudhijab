<?php

namespace App\Http\Requests;

// Need
use Illuminate\Foundation\Http\FormRequest;

class ObatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        // return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
                return [];
            case 'POST':
                return [
                    "kode_obat" => 'required|string|max:5',
                    "nama_obat" => 'required|string|min:10|max:250',
                    "harga" => 'required|integer|digits_between:1,6',
                    "jenis_obat" => 'required|in:Obat Ringan,Obat Berat,Obat Normal',
                ];
            case 'PUT':
                return [
                    "kode_obat" => 'required|string|max:5',
                    "nama_obat" => 'required|string|min:10|max:250',
                    "harga" => 'required|integer|digits_between:1,6',
                    "jenis_obat" => 'required|in:Obat Ringan,Obat Berat,Obat Normal',
                ];
            case 'PATCH':
                return [
                    "kode_obat" => 'string|max:5',
                    "nama_obat" => 'string|min:10|max:250',
                    "harga" => 'integer|digits_between:1,6',
                    "jenis_obat" => 'in:Obat Ringan,Obat Berat,Obat Normal',
                ];
            default:
                break;
        }

    }

    //// Need
    // public function failedValidation(Validator $validator)
    // {
    //     throw new HttpResponseException(response()->json([
    //         'success' => false,
    //         'message' => 'Validation errors',
    //         'data' => $validator->errors(),
    //     ]));
    // }

    // protected function failedValidation(Validator $validator)
    // {
    //     throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    // }

}
