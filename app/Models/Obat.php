<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Obat extends Model
{
    use HasFactory;

    use SoftDeletes;

    protected $table = 'data_obat';

    protected $fillable = ['kode_obat','nama_obat','jenis_obat','harga',];

    protected $hidden = ['deleted_at'];
}
