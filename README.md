# Welcome to ApiExampleCRUDHijab!

Pertama Clone dahulu Repo ini

    git clone git@gitlab.com:karapho/apiexamplecrudhijab.git
    
Lalu masuk ke folder nya

Buka terminal, lalu jalankan perintah 

    composer install

Selanjutnya jalankan perintah 

    npm install

dan

    npm run dev

Buat file .env dengan perintah

    cp .env.example .env

Lalu, Jalankan perintah

    php artisan key:generate

dan 

    php artisan jwt:secret

lalu atur koneksi ke database pada file .env yang telah terbuat sebelumnya

Untuk dummy datanya jalankan perintah berikut

    php artisan migrate --seed

Bila terjadi error karna data telah ada, tambahkan perintah fresh 

    php artisan migrate:fresh --seed

Aplikasi siap digunakan, untuk dokumentasi API nya bisa di lihat di
https://documenter.getpostman.com/view/10735379/UVJiiaGP

> NB: Saya menggunakan localhost:8050
> Silahkan di sesuaikan dengan url yang digunakan
